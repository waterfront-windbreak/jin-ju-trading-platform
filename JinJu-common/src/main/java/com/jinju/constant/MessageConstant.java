package com.jinju.constant;

public class MessageConstant {
    public static final String PASSWORD_ERROR="密码错误";
    public static final String USER_NOT_FOUND="账号不存在";
    public static final String NOT_LOGIN="未登录";
    public static final String ALREADY_EXISTS="用户已存在";
    public static final String UNKNOWN_ERROR="未知错误";
    public static final String SUCCESS_MESSAGE="请求成功";
    public static final String BALANCE="余额不足！";
    public static final Integer FINISH_STATUS=1;
    public static final Integer FAIL_STATUS=2;
    public static final String NOT_ENOUGH="库存不足";
}
