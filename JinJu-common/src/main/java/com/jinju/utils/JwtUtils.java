package com.jinju.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Map;

/**
 * Jwt工具类
 */
public class JwtUtils {
    //生成jwt令牌
    public static String createJWT(String secretKey, Long ttlMillis, Map<String, Object> claims){
        String jwt = Jwts.builder()
                .setClaims(claims)//设置传递内容
                .signWith(SignatureAlgorithm.HS256, secretKey.getBytes(StandardCharsets.UTF_8))//设置签名算法和密钥
                .setExpiration(new Date(System.currentTimeMillis() + ttlMillis))//设置过期时间
                .compact();
        return jwt;
    }

    //解析jwt令牌
    public static Claims parseJWT(String secretKet, String jwt){
        Claims claims = Jwts.parser()
                .setSigningKey(secretKet.getBytes(StandardCharsets.UTF_8))//设置密钥
                .parseClaimsJws(jwt)//解析jwt
                .getBody();
        return claims;
    }
}
