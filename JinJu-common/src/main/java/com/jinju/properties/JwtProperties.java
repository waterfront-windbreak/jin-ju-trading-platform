package com.jinju.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Jwt配置文件类
 */
@Component
@ConfigurationProperties(prefix = "jinju.jwt")
@Data
public class JwtProperties {
    private String peopleSecretKey;
    private long peopleTtl;
    private String peopleTokenName;
}
