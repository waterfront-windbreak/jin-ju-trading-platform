package com.jinju.exception;

/**
 * 余额不足异常
 */
public class BalanceException extends BaseException{
    public BalanceException() {
        super();
    }

    public BalanceException(String msg) {
        super(msg);
    }
}
