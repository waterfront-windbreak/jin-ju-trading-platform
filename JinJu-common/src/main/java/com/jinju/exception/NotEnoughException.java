package com.jinju.exception;

/**
 * 库存不足异常
 */
public class NotEnoughException extends BaseException{
    public NotEnoughException() {
        super();
    }

    public NotEnoughException(String msg) {
        super(msg);
    }
}
