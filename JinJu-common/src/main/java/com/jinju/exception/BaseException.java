package com.jinju.exception;

/**
 * 基础异常
 */
public class BaseException extends RuntimeException{
    public BaseException(){}

    public BaseException(String msg){
        super(msg);
    }
}
