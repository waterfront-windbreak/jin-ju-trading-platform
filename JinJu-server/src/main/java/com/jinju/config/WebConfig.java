package com.jinju.config;

import com.jinju.interceptor.JwtInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 配置类设置拦截路径
 */
@Configuration
@Slf4j
public class WebConfig extends WebMvcConfigurationSupport {
    @Autowired
    private JwtInterceptor jwtInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //拦截除登录注册请求外所有请求
        registry.addInterceptor(jwtInterceptor).addPathPatterns("/**").excludePathPatterns("/people/login","/people/register");
    }

}
