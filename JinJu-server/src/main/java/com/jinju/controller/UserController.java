package com.jinju.controller;

import com.jinju.constant.JwtClaimsConstant;
import com.jinju.dto.*;
import com.jinju.entity.People;
import com.jinju.properties.JwtProperties;
import com.jinju.result.PageResult;
import com.jinju.result.Result;
import com.jinju.service.PeopleService;
import com.jinju.utils.JwtUtils;
import com.jinju.vo.PeopleLoginVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 人员管理控制类
 */
@RestController
@RequestMapping("/people")
@Slf4j
public class UserController {
    @Autowired
    private PeopleService peopleService;
    @Autowired
    private JwtProperties jwtProperties;

    /**
     * 人员登录
     * @param peopleLoginDTO
     * @return
     */
    @PostMapping("/login")
    public Result<PeopleLoginVO> login(PeopleLoginDTO peopleLoginDTO){
        log.info("人员登录：{}",peopleLoginDTO);
        People people = peopleService.login(peopleLoginDTO);
        Map<String,Object> claims=new HashMap<>();
        claims.put(JwtClaimsConstant.USER_ID,people.getId());//存放jwt中的内容
        log.info("用户id：{}",people.getId());
        String jwt = JwtUtils.createJWT(jwtProperties.getPeopleSecretKey(), jwtProperties.getPeopleTtl(), claims);//生成jwt令牌
        PeopleLoginVO peopleLoginVO=PeopleLoginVO.builder()//封装返回的数据
                .id(people.getId())
                .username(people.getUsername())
                .name(people.getName())
                .role(people.getRole())
                .token(jwt)
                .build();
        return Result.success(peopleLoginVO);
    }

    /**
     * 退出登录
     * @return
     */
    @PostMapping("/logout")
    public Result<String> logout(){
        return Result.success();
    }

    /**
     * 条件分页查询人员列表
     * @param peopleListDTO
     * @return
     */
    @GetMapping
    public Result<PageResult> getPeopleList(PeopleListDTO peopleListDTO){
        log.info("条件分页查询人员列表参数：{}",peopleListDTO);
        PageResult peopleList = peopleService.getPeopleList(peopleListDTO);
        return Result.success(peopleList);
    }

    /**
     * 根据id查询用户
     * 用于更新数据回显或查看自己资料
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<People> getPeopleListById(@PathVariable Long id){
        log.info("查询的用户id为：{}",id);
        People list = peopleService.getPeopleListById(id);
        return Result.success(list);
    }

    /**
     * 注销用户
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result<String> deletePeople(@PathVariable Long id){
        log.info("要注销的用户id为：{}",id);
        peopleService.deletePeople(id);
        return Result.success();
    }

    /**
     * 注册用户
     * @param addPeopleDTO
     * @return
     */
    @PostMapping("/register")
    public Result<String> addPeople(AddPeopleDTO addPeopleDTO){
        log.info("新增用户：{}",addPeopleDTO);
        peopleService.addPeople(addPeopleDTO);
        return Result.success();
    }

    /**
     * 更新用户信息
     * @param updatePeopleDTO
     * @return
     */
    @PutMapping
    public Result<String> updatePeople(UpdatePeopleDTO updatePeopleDTO){
        log.info("更新用户：{}",updatePeopleDTO);
        peopleService.updatePeople(updatePeopleDTO);
        return Result.success();
    }

    /**
     * 修改密码
     * @param updatePasswordDTO
     * @return
     */
    @PutMapping("/updatePassword")
    public Result<String> updatePassword(UpdatePasswordDTO updatePasswordDTO){
        log.info("修改密码：{}",updatePasswordDTO);
        peopleService.updatePassword(updatePasswordDTO);
        return Result.success();
    }

    /**
     * 账户充值
     * @param id
     * @param money
     * @return
     */
    @PostMapping("/addMoney")
    public Result<String> updateMoney(Long id,Double money){
        log.info("账户充值人：{}，充值金额：{}",id,money);
        peopleService.addMoney(id, money);
        return Result.success();
    }
}
