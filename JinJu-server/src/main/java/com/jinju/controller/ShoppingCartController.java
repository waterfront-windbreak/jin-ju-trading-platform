package com.jinju.controller;

import com.jinju.dto.AddShoppingCartDTO;
import com.jinju.dto.UpdateShoppingCartDTO;
import com.jinju.result.Result;
import com.jinju.service.ShoppingCartService;
import com.jinju.vo.ShoppingCartListVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     * @param addShoppingCartDTO
     * @return
     */
    @PostMapping
    public Result<String> addShoppingCart(AddShoppingCartDTO addShoppingCartDTO){
        log.info("新加购物车的信息：{}",addShoppingCartDTO);
        shoppingCartService.addShoppingCart(addShoppingCartDTO);
        return Result.success();
    }

    /**
     * 查看购物车列表
     * @param bId
     * @return
     */
    @GetMapping("/{bId}")
    public Result<List<ShoppingCartListVO>> getShoppingCartListById(@PathVariable Long bId){
        log.info("查询的用户id为：{}",bId);
        List<ShoppingCartListVO> list = shoppingCartService.getShoppingCartListById(bId);
        return Result.success(list);
    }

    /**
     * 批量删除购物车
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    public Result<String> deleteShoppingCart(@PathVariable Long[] ids){
        log.info("批量删除的购物车id：{}", Arrays.toString(ids));
        shoppingCartService.deleteShoppingCart(ids);
        return Result.success();
    }

    /**
     * 更新购物车
     * @param updateShoppingCartDTO
     * @return
     */
    @PutMapping
    public Result<String> updateShoppingCart(UpdateShoppingCartDTO updateShoppingCartDTO){
        log.info("更新购物车的信息为：{}",updateShoppingCartDTO);
        shoppingCartService.updateShoppingCart(updateShoppingCartDTO);
        return Result.success();
    }
}
