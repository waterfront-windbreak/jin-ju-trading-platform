package com.jinju.controller;

import com.jinju.result.Result;
import com.jinju.utils.AliOssUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 文件上传
 */
@RestController
@RequestMapping("/upload")
@Slf4j
public class UploadController {
    @Autowired
    private AliOssUtils aliOssUtils;
    @PostMapping
    public Result<String> upload(MultipartFile image) throws IOException {
        log.info("文件上传：{}",image.getOriginalFilename());
        String url = aliOssUtils.upload(image);
        return Result.success(url);
    }
}
