package com.jinju.controller;

import com.jinju.dto.AddGoodsDTO;
import com.jinju.dto.AdminGoodsListDTO;
import com.jinju.dto.UpdateGoodsDTO;
import com.jinju.dto.UserGoodsListDTO;
import com.jinju.entity.Goods;
import com.jinju.result.PageResult;
import com.jinju.result.Result;
import com.jinju.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 商品控制类
 */
@RestController
@RequestMapping("/goods")
@Slf4j
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 条件分页查询
     * @param userGoodsListDTO
     * @return
     */
    @GetMapping
    public Result<PageResult> getUserGoodsList(UserGoodsListDTO userGoodsListDTO){
        log.info("分页查询参数：{}",userGoodsListDTO);
        PageResult list = goodsService.getUserGoodsList(userGoodsListDTO);
        return Result.success(list);
    }

    /**
     * 分页查询
     * @param adminGoodsListDTO
     * @return
     */
    @GetMapping("/admingoods")
    public Result<PageResult> getAdminGoodsList(AdminGoodsListDTO adminGoodsListDTO){
        log.info("分页查询参数：{}",adminGoodsListDTO);
        PageResult list = goodsService.getAdminGoodsList(adminGoodsListDTO);
        return Result.success(list);
    }

    /**
     * 批量删除商品
     * @param ids
     * @return
     */
    @DeleteMapping("/{ids}")
    public Result<String> deleteGoods(@PathVariable Long[] ids){
        log.info("批量删除商品：{}", Arrays.toString(ids));
        goodsService.deleteGoods(ids);
        return Result.success();
    }

    /**
     * 新增商品
     * @param addGoodsDTO
     * @return
     */
    @PostMapping
    public Result<String> addGoods(AddGoodsDTO addGoodsDTO){
        log.info("添加商品：{}",addGoodsDTO);
        goodsService.addGoods(addGoodsDTO);
        return Result.success();
    }

    /**
     * 根据用户id查询商品
     * 用于用户查询自己上传的商品
     * @param pId
     * @return
     */
    @GetMapping("/pId")
    public Result<List<Goods>> getGoodsByPid(Long pId){
        log.info("当前用户id：{}",pId);
        List<Goods> list = goodsService.getGoodsByPid(pId);
        return Result.success(list);
    }

    /**
     * 根据id查询商品
     * 用于更新回显操作
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Goods> getGoodsById(@PathVariable Long id){
        log.info("当前商品id：{}",id);
        Goods goods = goodsService.getGoodsById(id);
        return Result.success(goods);
    }

    /**
     * 更新商品
     * @param updateGoodsDTO
     * @return
     */
    @PutMapping
    public Result<String> updateGoods(UpdateGoodsDTO updateGoodsDTO){
        log.info("更新商品信息为：{}",updateGoodsDTO);
        goodsService.updateGoods(updateGoodsDTO);
        return Result.success();
    }

    /**
     * 通过审核
     * @param id
     * @return
     */
    @GetMapping("/pass")
    public Result<String> passGoods(Long id){
        log.info("{}商品通过审核",id);
        goodsService.passGoods(id);
        return Result.success();
    }

    /**
     * 驳回商品
     * @param id
     * @return
     */
    @GetMapping("/return")
    public Result<String> returnGoods(Long id){
        log.info("{}商品被驳回",id);
        goodsService.returnGoods(id);
        return Result.success();
    }
}
