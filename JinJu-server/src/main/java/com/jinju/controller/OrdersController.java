package com.jinju.controller;

import com.jinju.dto.AddOrdersDTO;
import com.jinju.dto.SettlementOrdersDTO;
import com.jinju.dto.UpdateOrdersDTO;
import com.jinju.result.PageResult;
import com.jinju.result.Result;
import com.jinju.service.OrdersService;
import com.jinju.vo.OrderListVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 订单管理控制类
 */
@RestController
@Slf4j
@RequestMapping("/orders")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    /**
     * 创建订单
     * @param addOrdersDTO
     * @return
     */
    @PostMapping
    public Result<String> addOrders(AddOrdersDTO addOrdersDTO){
        log.info("创建订单：{}",addOrdersDTO);
        ordersService.addOrders(addOrdersDTO);
        return Result.success();
    }

    /**
     * 查看当前用户创建的订单
     * @param bId
     * @return
     */
    @GetMapping("/{bId}")
    public Result<List<OrderListVO>> getOrderListByBid(@PathVariable Long bId){
        log.info("要查询订单的用户id为：{}",bId);
        List<OrderListVO> list = ordersService.getOrderListByBid(bId);
        return Result.success(list);
    }

    /**
     * 批量删除订单
     * @param ids
     */
    @DeleteMapping("/{ids}")
    public Result<String> deleteOrders(@PathVariable Long[] ids){
        log.info("要删除的订单：{}", Arrays.toString(ids));
        ordersService.deleteOrders(ids);
        return Result.success();
    }

    /**
     * 更新订单
     * @param updateOrdersDTO
     */
    @PutMapping
    public Result<String> updateOrders(UpdateOrdersDTO updateOrdersDTO){
        log.info("更新的订单信息：{}",updateOrdersDTO);
        ordersService.updateOrders(updateOrdersDTO);
        return Result.success();
    }

    /**
     * 订单结算
     * @param settlementOrdersDTO
     * @return
     */
    @PostMapping("/buy")
    public Result<String> settlementOrders(SettlementOrdersDTO settlementOrdersDTO){
        log.info("订单结算信息：{}",settlementOrdersDTO);
        ordersService.settlementOrders(settlementOrdersDTO);
        return Result.success();
    }

    /**
     * 取消订单
     * @param id
     * @return
     */
    @GetMapping("/cancel")
    public Result<String> cancelOrders(Long id){
        log.info("取消的订单id：{}",id);
        ordersService.cancelOrders(id);
        return Result.success();
    }
}
