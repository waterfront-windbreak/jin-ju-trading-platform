package com.jinju.mapper;

import com.jinju.dto.UpdateOrdersDTO;
import com.jinju.entity.Orders;
import com.jinju.vo.OrderListVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 订单相关数据访问层
 */
@Mapper
public interface OrdersMapper {
    /**
     * 创建订单
     * @param orders
     */
    @Insert("insert into orders(total_price, g_id, b_id, s_id, number, create_time, finish_time) VALUES " +
            "(#{totalPrice},#{gId},#{bId},#{sId},#{number},#{createTime},#{finishTime})")
    void addOrders(Orders orders);

    /**
     * 查看当前用户创建的订单
     * @param bId
     * @return
     */
    List<OrderListVO> getOrderListByBid(Long bId);

    /**
     * 批量删除订单
     * @param ids
     */
    void deleteOrders(Long[] ids);

    /**
     * 更新订单
     * @param updateOrdersDTO
     */
    @Update("update orders set number=#{number},total_price=#{totalPrice} where id=#{id}")
    void updateOrders(UpdateOrdersDTO updateOrdersDTO);

    /**
     * 更新订单状态
     * @param id
     * @param status
     * @param finishTime
     */
    @Update("update orders set status=#{status},finish_time=#{finishTime} where id=#{id}")
    void updateStatus(Long id, Integer status, LocalDateTime finishTime);
}
