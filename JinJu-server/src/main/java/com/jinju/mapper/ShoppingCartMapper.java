package com.jinju.mapper;

import com.jinju.dto.AddShoppingCartDTO;
import com.jinju.dto.UpdateShoppingCartDTO;
import com.jinju.entity.ShoppingCart;
import com.jinju.vo.ShoppingCartListVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 购物车数据访问层
 */
@Mapper
public interface ShoppingCartMapper {
    /**
     * 添加购物车
     * @param addShoppingCartDTO
     */
    @Insert("insert into shoppingcart(g_id, b_id, s_id, number, total_price) VALUES " +
            "(#{gId},#{bId},#{sId},#{number},#{totalPrice})")
    void addShoppingCart(AddShoppingCartDTO addShoppingCartDTO);

    /**
     * 查询购物车列表
     * @param bId
     * @return
     */
    List<ShoppingCartListVO> getShoppingCartListById(Long bId);

    /**
     * 批量移出购物车
     * @param ids
     */
    void deleteShoppingCart(Long[] ids);

    /**
     * 更新购物车
     * @param updateShoppingCartDTO
     */
    @Update("update shoppingcart set number=#{number},total_price=#{totalPrice} where id=#{id}")
    void updateShoppingCart(UpdateShoppingCartDTO updateShoppingCartDTO);

    /**
     * 根据商品id删除购物车
     * @param gIds
     */
    void deleteShoppingCartByGid(Long[] gIds);

    /**
     * 根据用户id删除购物车
     * @param bId
     */
    @Delete("delete from shoppingcart where b_id=#{bId}")
    void deleteShoppingCartByBid(Long bId);
}
