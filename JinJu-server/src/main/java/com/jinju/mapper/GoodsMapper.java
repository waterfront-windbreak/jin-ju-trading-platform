package com.jinju.mapper;

import com.jinju.dto.AdminGoodsListDTO;
import com.jinju.dto.UpdateGoodsDTO;
import com.jinju.dto.UserGoodsListDTO;
import com.jinju.entity.Goods;
import com.jinju.result.PageResult;
import com.jinju.vo.GoodsListVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 商品相关数据访问层
 */
@Mapper
public interface GoodsMapper {
    /**
     * 条件分页查询商品
     * @param userGoodsListDTO
     * @return
     */
    List<GoodsListVO> getUserGoodsList(UserGoodsListDTO userGoodsListDTO);

    /**
     * 分页查询所有待审核的商品列表
     */
    List<GoodsListVO> getAdminGoodsList(AdminGoodsListDTO adminGoodsListDTO);

    /**
     * 批量删除商品
     * @param ids
     */
    void deleteGoods(Long[] ids);

    /**
     * 上传商品
     * @param goods
     */
    @Insert("insert into goods(name, image, category, price, introduction, number, p_id, create_time, update_time) " +
            "VALUES (#{name},#{image},#{category},#{price},#{introduction},#{number},#{pId},#{createTime},#{updateTime})")
    void addGoods(Goods goods);

    /**
     * 根据用户id查询商品
     * 用于用户查询自己上传的所有商品
     * @param pId
     * @return
     */
    @Select("select * from goods where p_id=#{pId}")
    List<Goods> getGoodsByPid(Long pId);

    /**
     * 根据id查询商品
     * 用于更新回显
     * @param id
     * @return
     */
    @Select("select * from goods where id=#{id}")
    Goods getGoodsById(Long id);

    /**
     * 更新商品信息
     * @param updateGoodsDTO
     */
    void updateGoods(UpdateGoodsDTO updateGoodsDTO);

    /**
     * 更新库存
     * @param id
     * @param number
     * @param updateTime
     */
    @Update("update goods set number=#{number},update_time=#{updateTime} where id=#{id}")
    void updateNumber(Long id, Integer number, LocalDateTime updateTime);

    /**
     * 商品审核
     * @param id
     * @param status
     */
    @Update("update goods set status=#{status},update_time=#{updateTime} where id=#{id}")
    void updateStatus(Long id,Integer status,LocalDateTime updateTime);
}
