package com.jinju.mapper;

import com.jinju.dto.PeopleListDTO;
import com.jinju.entity.People;
import org.apache.ibatis.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 人员相关数据访问层
 */
@Mapper
public interface PeopleMapper {
    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from people where username=#{username}")
    People getByUsername(String username);

    /**
     * 条件分页查询人员列表
     * @param peopleListDTO
     * @return
     */
    List<People> getPeopleList(PeopleListDTO peopleListDTO);

    /**
     * 根据id查询用户
     * 用于更新数据回显或查看自己资料
     * @param id
     * @return
     */
    @Select("select * from people where id=#{id}")
    People getPeopleListById(Long id);

    /**
     * 用户注销
     * @param id
     */
    @Delete("delete from people where id=#{id}")
    void deletePeople(Long id);

    /**
     * 用户注册
     * @param people
     */
    @Insert("insert into people(username, password, create_time, update_time) VALUES " +
            "(#{username},#{password},#{createTime},#{updateTime})")
    void addPeople(People people);

    /**
     * 更新用户信息
     * @param people
     */
    void updatePeople(People people);

    /**
     * 根据id查询金额
     * @param id
     * @return
     */
    @Select("select money from people where id=#{id}")
    Double getMoneyById(Long id);

    /**
     * 更新金额
     * @param id
     * @param money
     */
    @Update("update people set money=#{money},update_time=#{updateTime} where id=#{id}")
    void updateMoney(Long id, Double money, LocalDateTime updateTime);
}
