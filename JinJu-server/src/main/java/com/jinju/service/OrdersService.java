package com.jinju.service;

import com.jinju.dto.AddOrdersDTO;
import com.jinju.dto.SettlementOrdersDTO;
import com.jinju.dto.UpdateOrdersDTO;
import com.jinju.vo.OrderListVO;

import java.util.List;

public interface OrdersService {
    void addOrders(AddOrdersDTO addOrdersDTO);
    List<OrderListVO> getOrderListByBid(Long bId);
    void deleteOrders(Long[] ids);
    void updateOrders(UpdateOrdersDTO updateOrdersDTO);
    void settlementOrders(SettlementOrdersDTO settlementOrdersDTO);
    void cancelOrders(Long id);
}
