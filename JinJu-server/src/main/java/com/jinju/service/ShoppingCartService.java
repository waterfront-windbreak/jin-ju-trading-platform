package com.jinju.service;

import com.jinju.dto.AddShoppingCartDTO;
import com.jinju.dto.UpdateShoppingCartDTO;
import com.jinju.vo.ShoppingCartListVO;

import java.util.List;

public interface ShoppingCartService {
    void addShoppingCart(AddShoppingCartDTO addShoppingCartDTO);
    List<ShoppingCartListVO> getShoppingCartListById(Long bId);
    void deleteShoppingCart(Long[] ids);
    void updateShoppingCart(UpdateShoppingCartDTO updateShoppingCartDTO);
}
