package com.jinju.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jinju.constant.MessageConstant;
import com.jinju.dto.*;
import com.jinju.entity.People;
import com.jinju.exception.BalanceException;
import com.jinju.exception.PasswordErrorException;
import com.jinju.exception.UserNotFoundException;
import com.jinju.mapper.PeopleMapper;
import com.jinju.mapper.ShoppingCartMapper;
import com.jinju.result.PageResult;
import com.jinju.service.PeopleService;
import com.jinju.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class PeopleServiceImpl implements PeopleService {
    @Autowired
    private PeopleMapper mapper;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 用户登录
     * @param peopleLoginDTO
     * @return
     */
    @Override
    public People login(PeopleLoginDTO peopleLoginDTO) {
        String username = peopleLoginDTO.getUsername();
        String password = peopleLoginDTO.getPassword();
        People people = mapper.getByUsername(username);
        //如果未查询到用户，返回“账户不存在”
        if (people==null){
            throw new UserNotFoundException(MessageConstant.USER_NOT_FOUND);
        }
        //将密码进行md5加密后进行比对
        password= DigestUtils.md5DigestAsHex(password.getBytes());
        log.info("密码：{}",password);
        //比对失败则返回“密码错误”
        if (!password.equals(people.getPassword())){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
        return people;
    }

    /**
     * 条件分页查询人员列表
     * @param peopleListDTO
     * @return
     */
    @Override
    public PageResult getPeopleList(PeopleListDTO peopleListDTO) {
        PageHelper.startPage(peopleListDTO.getPage(), peopleListDTO.getPageSize());
        List<People> list = mapper.getPeopleList(peopleListDTO);
        Page<People> page=(Page<People>) list;
        PageResult pageResult=new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 根据id查询用户
     * 用于更新数据回显或查看自己资料
     * @param id
     * @return
     */
    @Override
    public People getPeopleListById(Long id) {
        return mapper.getPeopleListById(id);
    }

    /**
     * 注销用户
     * @param id
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deletePeople(Long id) {
        mapper.deletePeople(id);
        //注销用户同时清空该用户的购物车
        shoppingCartMapper.deleteShoppingCartByBid(id);
    }

    /**
     * 注册用户
     * @param addPeopleDTO
     */
    @Override
    public void addPeople(AddPeopleDTO addPeopleDTO) {
        String password = addPeopleDTO.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());//将密码进行md5加密
        People people=new People();
        BeanUtils.copyProperties(addPeopleDTO,people);
        people.setCreateTime(LocalDateTime.now());
        people.setUpdateTime(LocalDateTime.now());
        people.setPassword(password);
        mapper.addPeople(people);
    }

    /**
     * 更新用户信息
     * @param updatePeopleDTO
     */
    @Override
    public void updatePeople(UpdatePeopleDTO updatePeopleDTO) {
        People people=new People();
        BeanUtils.copyProperties(updatePeopleDTO,people);
        people.setUpdateTime(LocalDateTime.now());
        mapper.updatePeople(people);
    }

    /**
     * 修改密码
     * @param updatePasswordDTO
     */
    @Override
    public void updatePassword(UpdatePasswordDTO updatePasswordDTO) {
        String password = DigestUtils.md5DigestAsHex(updatePasswordDTO.getOriginPassword().getBytes());
        People people = mapper.getPeopleListById(updatePasswordDTO.getId());//查询要修改密码的用户
        //将原密码进行比对
        if (password.equals(people.getPassword())){
            String newPassword = DigestUtils.md5DigestAsHex(updatePasswordDTO.getNewPassword().getBytes());
            people.setPassword(newPassword);
            mapper.updatePeople(people);
        }else {
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }
    }

    /**
     * 账户充值
     * @param id
     * @param money
     */
    @Override
    public void addMoney(Long id, Double money) {
        Double currentMoney = mapper.getMoneyById(id);
        mapper.updateMoney(id, money+currentMoney,LocalDateTime.now());
    }

    /**
     * 账户扣费
     * @param id
     * @param money
     */
    @Override
    public void deductingMoney(Long id, Double money) {
        Double currentMoney = mapper.getMoneyById(id);
        if ((currentMoney-money)<0){
            throw new BalanceException(MessageConstant.BALANCE);
        }
        mapper.updateMoney(id, currentMoney-money,LocalDateTime.now());
    }

}