package com.jinju.service.impl;

import com.jinju.constant.MessageConstant;
import com.jinju.context.BaseContext;
import com.jinju.dto.AddOrdersDTO;
import com.jinju.dto.SettlementOrdersDTO;
import com.jinju.dto.UpdateGoodsDTO;
import com.jinju.dto.UpdateOrdersDTO;
import com.jinju.entity.Goods;
import com.jinju.entity.Orders;
import com.jinju.exception.NotEnoughException;
import com.jinju.mapper.GoodsMapper;
import com.jinju.mapper.OrdersMapper;
import com.jinju.mapper.PeopleMapper;
import com.jinju.service.GoodsService;
import com.jinju.service.OrdersService;
import com.jinju.service.PeopleService;
import com.jinju.vo.OrderListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private OrdersMapper ordersMapper;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private PeopleService peopleService;

    /**
     * 创建订单
     * @param addOrdersDTO
     */
    @Override
    public void addOrders(AddOrdersDTO addOrdersDTO) {
        Orders orders=new Orders();
        BeanUtils.copyProperties(addOrdersDTO,orders);
        orders.setBId(BaseContext.getCurrentId());
        orders.setCreateTime(LocalDateTime.now());
        System.out.println(BaseContext.getCurrentId()+"---------");
        ordersMapper.addOrders(orders);
    }

    /**
     * 查看当前用户创建的订单
     * @param bId
     * @return
     */
    @Override
    public List<OrderListVO> getOrderListByBid(Long bId) {
        return ordersMapper.getOrderListByBid(bId);
    }

    /**
     * 批量删除订单
     * @param ids
     */
    @Override
    public void deleteOrders(Long[] ids) {
        ordersMapper.deleteOrders(ids);
    }

    /**
     * 更新订单
     * @param updateOrdersDTO
     */
    @Override
    public void updateOrders(UpdateOrdersDTO updateOrdersDTO) {
        ordersMapper.updateOrders(updateOrdersDTO);
    }

    /**
     * 订单结算
     * @param settlementOrdersDTO
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void settlementOrders(SettlementOrdersDTO settlementOrdersDTO) {
        //修改该订单的状态
        ordersMapper.updateStatus(settlementOrdersDTO.getId(), MessageConstant.FINISH_STATUS,LocalDateTime.now());
        //更新对应商品的库存
        goodsService.updateNumber(settlementOrdersDTO.getGId(),settlementOrdersDTO.getNumber());
        //为卖家增加对应金额
        peopleService.addMoney(settlementOrdersDTO.getSId(), settlementOrdersDTO.getTotalPrice());
        //为买家扣除对应金额
        peopleService.deductingMoney(BaseContext.getCurrentId(),settlementOrdersDTO.getTotalPrice());
    }

    /**
     * 取消订单
     * @param id
     */
    @Override
    public void cancelOrders(Long id) {
        ordersMapper.updateStatus(id,MessageConstant.FAIL_STATUS,LocalDateTime.now());
    }
}
