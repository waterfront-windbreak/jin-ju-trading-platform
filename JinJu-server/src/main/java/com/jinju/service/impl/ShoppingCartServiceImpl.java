package com.jinju.service.impl;

import com.jinju.context.BaseContext;
import com.jinju.dto.AddShoppingCartDTO;
import com.jinju.dto.UpdateShoppingCartDTO;
import com.jinju.entity.ShoppingCart;
import com.jinju.mapper.ShoppingCartMapper;
import com.jinju.service.ShoppingCartService;
import com.jinju.vo.ShoppingCartListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 添加购物车
     * @param addShoppingCartDTO
     */
    @Override
    public void addShoppingCart(AddShoppingCartDTO addShoppingCartDTO) {
        addShoppingCartDTO.setBId(BaseContext.getCurrentId());
        shoppingCartMapper.addShoppingCart(addShoppingCartDTO);
    }

    /**
     * 查看购物车列表
     * @param bId
     * @return
     */
    @Override
    public List<ShoppingCartListVO> getShoppingCartListById(Long bId) {
        return shoppingCartMapper.getShoppingCartListById(bId);
    }

    /**
     * 批量移出购物车
     * @param ids
     */
    @Override
    public void deleteShoppingCart(Long[] ids) {
        shoppingCartMapper.deleteShoppingCart(ids);
    }

    /**
     * 更新购物车
     * @param updateShoppingCartDTO
     */
    @Override
    public void updateShoppingCart(UpdateShoppingCartDTO updateShoppingCartDTO) {
        shoppingCartMapper.updateShoppingCart(updateShoppingCartDTO);
    }


}
