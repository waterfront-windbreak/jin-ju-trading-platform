package com.jinju.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jinju.constant.MessageConstant;
import com.jinju.dto.AddGoodsDTO;
import com.jinju.dto.AdminGoodsListDTO;
import com.jinju.dto.UpdateGoodsDTO;
import com.jinju.dto.UserGoodsListDTO;
import com.jinju.entity.Goods;
import com.jinju.exception.NotEnoughException;
import com.jinju.mapper.GoodsMapper;
import com.jinju.mapper.ShoppingCartMapper;
import com.jinju.result.PageResult;
import com.jinju.service.GoodsService;
import com.jinju.service.ShoppingCartService;
import com.jinju.vo.GoodsListVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;

    /**
     * 条件分页查询
     * @param userGoodsListDTO
     * @return
     */
    @Override
    public PageResult getUserGoodsList(UserGoodsListDTO userGoodsListDTO) {
        PageHelper.startPage(userGoodsListDTO.getPage(), userGoodsListDTO.getPageSize());
        List<GoodsListVO> goods = goodsMapper.getUserGoodsList(userGoodsListDTO);
        Page<GoodsListVO> page=(Page<GoodsListVO>) goods;
        PageResult pageResult=new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 分页查询
     * @param adminGoodsListDTO
     * @return
     */
    @Override
    public PageResult getAdminGoodsList(AdminGoodsListDTO adminGoodsListDTO) {
        PageHelper.startPage(adminGoodsListDTO.getPage(), adminGoodsListDTO.getPageSize());
        List<GoodsListVO> goods = goodsMapper.getAdminGoodsList(adminGoodsListDTO);
        Page<GoodsListVO> page=(Page<GoodsListVO>) goods;
        PageResult pageResult=new PageResult(page.getTotal(), page.getResult());
        return pageResult;
    }

    /**
     * 批量删除商品
     * @param ids
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteGoods(Long[] ids) {
        goodsMapper.deleteGoods(ids);
        //下架商品同时移出用户该商品的购物车
        shoppingCartMapper.deleteShoppingCartByGid(ids);
    }

    /**
     * 添加商品
     * @param addGoodsDTO
     */
    @Override
    public void addGoods(AddGoodsDTO addGoodsDTO) {
        Goods goods=new Goods();
        BeanUtils.copyProperties(addGoodsDTO,goods);//属性拷贝
        goods.setCreateTime(LocalDateTime.now());//设置当前系统时间
        goods.setUpdateTime(LocalDateTime.now());
        goodsMapper.addGoods(goods);
    }

    /**
     * 根据用户id查询商品
     * 用于用户查询自己上传的商品
     * @param pId
     * @return
     */
    @Override
    public List<Goods> getGoodsByPid(Long pId) {
        List<Goods> list = goodsMapper.getGoodsByPid(pId);
        return list;
    }

    /**
     * 根据id查询商品
     * 用于更新回显
     * @param id
     * @return
     */
    @Override
    public Goods getGoodsById(Long id) {
        Goods goods = goodsMapper.getGoodsById(id);
        return goods;
    }

    /**
     * 更新商品
     * @param updateGoodsDTO
     */
    @Override
    public void updateGoods(UpdateGoodsDTO updateGoodsDTO) {
        updateGoodsDTO.setUpdateTime(LocalDateTime.now());
        goodsMapper.updateGoods(updateGoodsDTO);
    }

    /**
     * 更新库存
     * @param id
     * @param number
     */
    @Override
    public void updateNumber(Long id, Integer number) {
        Goods goods = goodsMapper.getGoodsById(id);
        if ((goods.getNumber()-number)<0){
            throw new NotEnoughException(MessageConstant.NOT_ENOUGH);
        }
        goodsMapper.updateNumber(id,goods.getNumber()-number,LocalDateTime.now());
    }

    /**
     * 通过审核
     * @param id
     */
    @Override
    public void passGoods(Long id) {
        goodsMapper.updateStatus(id,MessageConstant.FINISH_STATUS,LocalDateTime.now());
    }

    /**
     * 被驳回
     * @param id
     */
    @Override
    public void returnGoods(Long id) {
        goodsMapper.updateStatus(id,MessageConstant.FAIL_STATUS,LocalDateTime.now());
    }
}
