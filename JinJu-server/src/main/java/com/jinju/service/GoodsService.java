package com.jinju.service;

import com.jinju.dto.AddGoodsDTO;
import com.jinju.dto.AdminGoodsListDTO;
import com.jinju.dto.UpdateGoodsDTO;
import com.jinju.dto.UserGoodsListDTO;
import com.jinju.entity.Goods;
import com.jinju.result.PageResult;

import java.util.List;

public interface GoodsService {
    PageResult getUserGoodsList(UserGoodsListDTO userGoodsListDTO);
    PageResult getAdminGoodsList(AdminGoodsListDTO adminGoodsListDTO);
    void deleteGoods(Long[] ids);
    void addGoods(AddGoodsDTO addGoodsDTO);
    List<Goods> getGoodsByPid(Long pId);
    Goods getGoodsById(Long id);
    void updateGoods(UpdateGoodsDTO updateGoodsDTO);
    void updateNumber(Long id,Integer number);
    void passGoods(Long id);
    void returnGoods(Long id);
}
