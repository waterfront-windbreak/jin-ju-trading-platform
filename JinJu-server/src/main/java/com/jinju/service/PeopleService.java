package com.jinju.service;

import com.jinju.dto.*;
import com.jinju.entity.People;
import com.jinju.result.PageResult;

import java.util.List;

public interface PeopleService {
    People login(PeopleLoginDTO peopleLoginDTO);
    PageResult getPeopleList(PeopleListDTO peopleListDTO);
    People getPeopleListById(Long id);
    void deletePeople(Long id);
    void addPeople(AddPeopleDTO addPeopleDTO);
    void updatePeople(UpdatePeopleDTO updatePeopleDTO);
    void updatePassword(UpdatePasswordDTO updatePasswordDTO);
    void addMoney(Long id,Double money);
    void deductingMoney(Long id,Double money);
}
