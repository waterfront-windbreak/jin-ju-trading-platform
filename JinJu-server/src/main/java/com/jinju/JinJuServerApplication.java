package com.jinju;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JinJuServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JinJuServerApplication.class, args);
    }

}
