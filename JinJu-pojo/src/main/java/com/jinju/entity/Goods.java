package com.jinju.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 商品类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Goods implements Serializable {
    private static final long serialVersionUID=1L;
    private Long id;//商品id
    private String name;//商品名称
    private String image;//商品图片
    private String category;//商品类别
    private Double price;//价格
    private String introduction;//简介
    private Integer status;//状态：0 待审核，1 已通过 2 已下架
    private Integer number;//库存
    private Integer pId;//用户Id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;//修改时间
}
