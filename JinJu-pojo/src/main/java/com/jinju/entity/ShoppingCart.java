package com.jinju.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 购物车实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShoppingCart implements Serializable {
    private static final long serialVersionUID=1L;
    private Long id;//购物车id
    private Double totalPrice;//总价
    private Long gId;//商品id
    private Long bId;//买家id
    private Long sId;//卖家id
    private Integer number;//购买数量
}
