package com.jinju.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 人员实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class People implements Serializable {
    private static final long serialVersionUID=1L;
    private Long id;//人员id
    private String username;//用户名
    private String password;//密码
    private String name;//姓名
    private Integer gender;//性别
    private String image;//头像
    private String phone;//联系电话
    private Double money;//钱包
    private Integer role;//角色
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;//更新时间
}
