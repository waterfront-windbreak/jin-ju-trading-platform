package com.jinju.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 订单实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Orders implements Serializable {
    private static final long serialVersionUID=1L;
    private Long id;//订单id
    private Double totalPrice;//总价
    private Integer status;//状态
    private Long gId;//商品id
    private Long bId;//买家id
    private Long sId;//卖家id
    private Integer number;//购买数量
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;//创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime finishTime;//完成时间
}
