package com.jinju.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 查询商品返回的数据模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GoodsListVO implements Serializable {
    private Long gId;//商品id
    private String gName;//商品名称
    private String gImage;//商品图像
    private String category;//商品类别
    private Double price;//价格
    private String introduction;//简介
    private Integer status;//状态：0 待审核，1 已通过 2 已下架
    private Integer number;//库存
    private Long sId;//上传者id
    private String username;//用户昵称
    private String phone;//联系电话
}
