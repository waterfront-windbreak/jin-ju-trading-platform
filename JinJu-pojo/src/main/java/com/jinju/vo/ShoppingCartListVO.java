package com.jinju.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ShoppingCartListVO implements Serializable {
    private Long id;//订单id
    private String gImage;//商品图像
    private String gName;//商品名称
    private Double price;//价格
    private Integer sNumber;//购买数量
    private Double totalPrice;//总价
    private String username;//卖家昵称
    private Long sId;//卖家id
    private Long gId;//商品id
}
