package com.jinju.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 查询订单时返回的数据模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderListVO implements Serializable {
    private Long id;//订单id
    private String gImage;//商品图像
    private String gName;//商品名称
    private Double price;//价格
    private Integer oNumber;//购买数量
    private Double totalPrice;//总价
    private Long sId;//卖家id
    private Long gId;//商品id
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String createTime;//创建时间
    private Integer status;//状态
}
