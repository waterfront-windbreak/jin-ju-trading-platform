package com.jinju.vo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 登录返回的数据模型
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PeopleLoginVO implements Serializable {
    private Long id;
    private String username;
    private String name;
    private Integer role;
    private String token;
}
