package com.jinju.dto;


import lombok.Data;

import java.io.Serializable;

/**
 * 用户登录传递的数据模型
 */
@Data
public class PeopleLoginDTO implements Serializable {
    private String username;
    private String password;
}
