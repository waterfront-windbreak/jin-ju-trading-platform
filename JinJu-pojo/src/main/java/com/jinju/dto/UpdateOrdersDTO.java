package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 更新订单时传递的数据模型
 */
@Data
public class UpdateOrdersDTO implements Serializable {
    private Long id;//订单id
    private Integer number;//购买数量
    private Double totalPrice;//总价
}
