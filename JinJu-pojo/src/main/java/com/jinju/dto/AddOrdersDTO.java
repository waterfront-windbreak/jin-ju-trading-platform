package com.jinju.dto;

import lombok.Data;



/**
 * 创建订单传递的数据模型
 */
@Data
public class AddOrdersDTO {
    private Long gId;//商品id
    private Integer number;//购买数量
    private Double totalPrice;//总价
    private Long bId;//买家id
    private Long sId;//卖家id
}
