package com.jinju.dto;

import lombok.Data;

/**
 * 修改密码传递的数据模型
 */
@Data
public class UpdatePasswordDTO {
    private Long id;//用户id
    private String originPassword;//原密码
    private String newPassword;//新密码
}
