package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 条件分页查询员工列表时传递的数据模型
 */
@Data
public class PeopleListDTO implements Serializable {
    private String username;//用户名
    private String name;//姓名
    private Integer gender;//性别：0 男 1 女
    private Integer page=1;//当前页码
    private Integer pageSize=10;//每页记录数
}
