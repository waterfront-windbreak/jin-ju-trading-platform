package com.jinju.dto;

import com.jinju.result.PageResult;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.bind.DefaultValue;

import java.io.Serializable;

/**
 * 商品列表查询传递的数据模型
 */
@Data
public class UserGoodsListDTO implements Serializable {
    private String name;//商品名称
    private String category;//商品类别
    private Integer page=1;//当前页码
    private Integer pageSize=10;//每页记录数
}
