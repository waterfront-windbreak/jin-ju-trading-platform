package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 管理员商品列表查询
 */
@Data
public class AdminGoodsListDTO implements Serializable {
    private Integer page=1;//当前页码
    private Integer pageSize=10;//每页记录数
}
