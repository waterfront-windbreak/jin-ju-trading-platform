package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 订单结算时传递的数据模型
 */
@Data
public class SettlementOrdersDTO implements Serializable {
    private Long id;//订单id
    private Integer number;//购买数量
    private Double totalPrice;//总价
    private Long gId;//商品id
    private Long bId;//买家id
    private Long sId;//卖家id
}
