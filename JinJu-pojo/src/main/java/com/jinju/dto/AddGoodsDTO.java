package com.jinju.dto;

import lombok.Data;

/**
 * 添加商品时的数据模型
 */
@Data
public class AddGoodsDTO {
    private String name;//商品名称
    private String image;//商品图像
    private String category;//商品类别
    private Double price;//价格
    private String introduction;//简介
    private Integer number;//库存
    private Integer pId;//用户id
}
