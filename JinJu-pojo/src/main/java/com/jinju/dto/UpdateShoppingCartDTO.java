package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateShoppingCartDTO implements Serializable {
    private Long id;//订单id
    private Integer number;//购买数量
    private Double totalPrice;//总价
}
