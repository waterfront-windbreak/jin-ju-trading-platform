package com.jinju.dto;

import lombok.Data;

/**
 * 用户更新信息时传递的数据模型
 */
@Data
public class UpdatePeopleDTO {
    private Long id;
    private String username;//用户名
    private String name;//姓名
    private Integer gender;//性别
    private String image;//头像
    private String phone;//联系电话
}
