package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户注册时传递的数据模型
 */
@Data
public class AddPeopleDTO implements Serializable {
    private String username;//用户名
    private String password;//密码
}
