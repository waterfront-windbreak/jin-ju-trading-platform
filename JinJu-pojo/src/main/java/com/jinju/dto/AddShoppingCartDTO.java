package com.jinju.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 添加到购物车时传递的数据模型
 */
@Data
public class AddShoppingCartDTO implements Serializable {
    private Long gId;//商品id
    private Integer number;//购买数量
    private Double totalPrice;//总价
    private Long bId;//买家id
    private Long sId;//卖家id
}
